# README #


### What is this repository for? ###
* Dutch and German language files for Smartwaiver frontend

### Roadmap / planning ###
* Some trial and error is expected: we expect that final will probably be version 1.3
* Version 1.0
* Version 1.1 updated Dutch text (30-03-2018)

### Details ###
* Language use is informal and not formal
* Use of captials (LIKE THIS) is considered extremely rude and therefore uncommon in The Netherlands and in Germany. Where capital characters have been used in the original text those words have been underlined in the translation as is the normal practice locally (using <u></u>).

### Who do I talk to? ###

* Paracentrum Texel
- Rob Hopster
- rob@paracentrumtexel.nl
- +31619902694